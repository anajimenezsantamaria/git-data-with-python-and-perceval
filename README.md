# Git data with Python and Perceval

This project has a python script to gather data from any organization in github. The code uses [perceval](https://github.com/chaoss/grimoirelab-perceval) as a python library  to gather all commit activity from the git repositories in a given organization and gives the result in a json file.

I'm runing the code using Unity-tecnologies Organization and filtering by author and commit activity. 

## Next steps

This script is the first part needed to build my graph network analysis using networX and D3.js. The project is part of my MS Data Science subject: Grapph Analysis