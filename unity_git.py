
import requests
import time
import json
import shutil

import logging
logging.basicConfig(level=logging.INFO)

from perceval.backends.core.git import Git

data = {}

def extract_git_identities(repository):
    git_path = '/tmp/'+repository.split('/')[-1]
    repo = Git(uri=repository, gitpath= git_path)

    # fetch all commits as an iterator, and iterate it printing each hash
    logging.info("Retrieving data from {} repository".format(repository))
    for commit in repo.fetch():
        username = commit['data']['Author']
        # if username is not in the data, then you add the username and the repo name
        if username not in data.keys():
            logging.info("Adding {} user".format(username))
            data[username] = []
            data[username].append(repository)
        elif repository not in data[username]:
            logging.info("Adding {} to {}".format(repository, username))
            data[username].append(repository)

    shutil.rmtree(git_path)

def owner_repositories(name):
    query = "org:{}".format(name)
    page = 1
    repositories = []

    r = requests.get('https://api.github.com/search/repositories?q={}&page={}'.format(query, page))
    items = r.json()['items']

    while len(items) > 0:
        # github API has rate limit. that's why you ask for a 5 min inactivity
        time.sleep(5)
        for item in items:
            logging.info("Adding {} repository".format(item['clone_url']))
            repositories.append(item['clone_url'])
        page += 1
        r = requests.get('https://api.github.com/search/repositories?q={}&page={}'.format(query, page))
        items = r.json()['items']

    return repositories


def main():
    #repos = owner_repositories('tidelift')
    repos = owner_repositories('unity-technologies')
    #repos = ["https://github.com/Unity-Technologies/ScriptableRenderPipeline.git"]
    i = 1

    for repo in repos:
        try:
            logging.info("Analyzing repo number {} of {}".format(i,len(repos)))
            extract_git_identities(repo)
            i += 1
        except Exception:
            logging.info("Something went wrong!!!!!!!!!!")
            continue

    logging.info("Creating data.json file")
    with open ('data.json', 'w') as json_file:
        json.dump(data, json_file)

if __name__ == "__main__":
    main()
